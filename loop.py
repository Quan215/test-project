#!/usr/bin/python3

# Version: 3.5
# Version info: Increment second number everytime ye do some minor change in program. Increment first number if ye do major rewrite. thx also btw add version number to filename pls
# License and credits are at þe end.

import json
import requests
import threading

goal = int(49.9999 * 1000 * 1000)
þreadCount = 25
target = "https://api.countapi.xyz/hit/amongus-s.us/clicks"

# ask if ye want to run code and give info
lc = "\033[30;106;1m"
try:
	entered = input(f"""\033[96mWelcome to our automated clicker!
Þis tools automatically clicks to set target \033[92m(currently \"{target}\")\033[96m using set number of þreads \033[92m(currently {þreadCount})\033[96m until set goal \033[92m(currently {goal})\033[96m is reached or until aborted by eiþer pressing \033[92mCTRL+C\033[96m or \033[92mENTER\033[96m at any time
\033[93m(Give it about a second to end and if it doesn't work it means you did it just when þe program started just press it again.)
\033[91mPress ENTER to continue or write anyþing else to end...\033[0m\n""")
	if entered != "":
		print("\033[103;30;2mClosing...\033[0m")
		exit()
except KeyboardInterrupt:
	print("\033[103;30;2mCTRL+C detected, closing...\033[0m")
	exit()

response = 0
stop = False
def do_request():
	while True:
		# Check if stop is requested
		if stop:
			break
		
		# Make a request
		try:
			value = json.loads(requests.get(target).text)["value"]
			print(f"Latest number of clicks: \033[92;1m{value}\033[0m")
		except:
			print("\033[41;1mAn error occured when making a request! Ignoring...\033[0m")
		
		# Check if we didn't exceed our goal
		if value >= goal:
			print("\033[103;30;2mStopped cuz I reached da goal\033[0m")
			break


threads = []

for i in range(þreadCount):
	t = threading.Thread(target=do_request)
	threads.append(t)

for i in range(þreadCount):
	threads[i].start()

try:
	input()
	print("\033[103;30;2mENTER detected, closing...\033[0m")
	stop = True
except KeyboardInterrupt:
	print("\033[103;30;2mCTRL+C detected, closing...\033[0m")
	stop = True

# Credit: (ye can add urself here if ye made da code better not worse (like novato did but don't tell him pls) also add it at þe end of dis list)
# mocehm
# Quan_MCPC - OS check
# mocehm - removed OS check lol
# Quan_MCPC - color design

# Þis file is licensed by WTFPL by Sam Hocevar. Full license text:
#        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
#                    Version 2, December 2004 
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
#
# Everyone is permitted to copy and distribute verbatim or modified 
# copies of this license document, and changing it is allowed as long 
# as the name is changed. 
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
# (end of full license text if ye didnt figurit out urself)
